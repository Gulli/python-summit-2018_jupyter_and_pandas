# Python Summit 2018 - Daten analysieren und transformieren mit Python

Klonen Sie dieses Repository via ```git clone git@gitlab.com:mrmatsumoto/python-summit-2018_jupyter_and_pandas.git```. 

Nun haben Sie drei Möglichkeiten. 

1. Nutzen Sie die angegebenen Docker Compose Konfigurationen.
2. Nutzen Sie Vagrant.
3. Installieren Sie die notwendigen Pakete manuell auf Ihrem Rechner.

## Option 1 - Docker

Wenn Docker und Docker-Compose auf Ihrem System installiert ist, starten Sie die Services über:

```docker-compose -p summit up -d```

Jupyter wird im Container ```summit_summit_1``` automatisch gestartet und ist unter ```http://localhost:8888``` im Browser erreichbar. Das Passwort ist ```secret```.

## Option 2 - Vagrant

Für diese Option benötigen Sie Vagrant mit VirtualBox als Provider auf Ihrem Rechner. 

### VBox Plugin installieren (nur einmal notwendig)

```vagrant plugin install vagrant-vbguest```

### Vagrant starten

```vagrant up```

### Pakete installieren lassen (nur einmal notwendig)

```vagrant provision```

### Jupyter starten

Nachdem alles installiert wurde, können Sie die Maschine mit ```vagrant ssh``` betreten und Jupyter mit dem Befehl ```jupyter notebook``` starten. Jupyter ist dann in Ihrem Browser unter ```http://localhost:8888``` erreichbar.

## Option 3 - Manuelle Installation

### Requirements

* python 3.7
* conda
* jupyter 4.4.0
* wichtige python packages
    * pandas-0.23.4 
    * numpy-1.15.1
    * matplotlib-2.2.3
* zusätzlich python packages
    * mysql-connector-python-8.0.12
    * sqlalchemy-1.2.11
    * mysqlclient-1.3.13
    * weitere Python Packages werden während des Workshops installiert   
* git
* gcc (optional)

### Beispiel für eine Installation auf einem auf Debian basierenden Linux

#### Installation der notwendigen Pakete

```
apt-get update && \
apt-get install -y \
    bzip2 \
    git \
    wget
```

#### Installation von Conda

```
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh
```

Nun muss das Terminal neu gestartet werden, da in der ```.bash_rc``` das Verzeichnis ```$HOME/miniconda3/bin``` zur PATH Variable hinzugefügt wurden ist.

#### Installation der Python Packages

```
conda install -y jupyter
conda install -y pandas
conda install -y matplotlib

conda install -y mysql-connector-python
conda install -y sqlalchemy
conda install -y mysqlclient
```

#### Jupyter starten

```jupyter notebook```