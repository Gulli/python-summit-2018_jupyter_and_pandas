import os
from IPython.lib import passwd

if 'PASSWORD' in os.environ:
  password = os.environ['PASSWORD']
  if password:
    c.NotebookApp.password = passwd(password)
  else:
    c.NotebookApp.token = ''
    c.NotebookApp.password = ''
  del os.environ['PASSWORD']

## The IP address the notebook server will listen on.
c.NotebookApp.ip = '*'

## Whether to open in a browser after starting. The specific browser used is
#  platform dependent and determined by the python standard library `webbrowser`
#  module, unless it is overridden using the --browser (NotebookApp.browser)
#  configuration option.
c.NotebookApp.open_browser = False

## The name of the default kernel to start
c.MultiKernelManager.default_kernel_name = 'python3'