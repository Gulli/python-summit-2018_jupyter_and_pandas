import unittest
from unittest_data_provider import data_provider
import wolfram_parser


class TestWolframParser(unittest.TestCase):

    time_quantity_samples = lambda: (
        ('[20, "Weeks"]', 12096000.0),
        ('[21, "Minutes"]', 1260),
        ('[0.25, "Hours"]', 900),
        ('[1/10, "Seconds"]', 0.1),
        ('[MixedMagnitude[{1, 1}], MixedUnit[{"Hours", "Seconds"}]]', 3601),
        ('[MixedMagnitude[{2, 10}], MixedUnit[{"Hours", "Minutes"}]', 7800)
    )

    date_samples = lambda: (
        (
            '[{2015, 3, 1, 23, 50, 5.}, "Instant", "Gregorian", 0.]',
            {
                'year': 2015,
                'month': 3,
                'day': 1,
                'hour': 23,
                'minute': 50,
                'second': 5.0,
                'type_a': 'Instant',
                'calendar_type': 'Gregorian',
                'additional_value': 0.0
            }
        ),
    )

    entity_samples = lambda: (
        (
            '["AdministrativeDivision", {"California", "UnitedStates"}]',
            {'entity_type': 'AdministrativeDivision', 'values': ['California', 'UnitedStates']}
        ),
    )

    geo_position_samples = lambda: (
        (
            '[{38.6158706, -89.0540892}]',
            {'lat': '38.6158706', 'lon': '-89.0540892'}
        ),
    )

    @data_provider(time_quantity_samples)
    def test_parse_time_quantity(self, input, expected_result):
        self.assertEqual(wolfram_parser.parse_time_quantity(input), expected_result)

    @data_provider(date_samples)
    def test_parse_date(self, input, expected_result):
        self.assertEqual(wolfram_parser.parse_date(input), expected_result)

    @data_provider(entity_samples)
    def test_parse_entity(self, input, expected_result):
        self.assertEqual(wolfram_parser.parse_entity(input), expected_result)

    @data_provider(geo_position_samples)
    def test_parse_geo_position(self, input, expected_result):
        self.assertEqual(wolfram_parser.parse_geo_position(input), expected_result)


if __name__ == '__main__':
    unittest.main()