import re
from functools import reduce

mixed_unit_replacements = {
    '"Years"': 3600 * 24 * 364,
    '"Months"': 3600 * 24 * 30.5,
    '"Weeks"': 3600 * 24 * 7,
    '"Days"': 3600 * 24,
    '"Hours"': 3600,
    '"Minutes"': 60,
    '"Seconds"': 1
}


def parse_time_quantity(s):
    if s.find('MixedMagnitude') > -1:
        v = re.match(r'\[MixedMagnitude\[\{(([^\}]*))', s).group(1).replace(' ', '').split(',')
        u = re.match(r'\[(.*)MixedUnit\[\{(([^\}]*))', s).group(2).replace(' ', '').split(',')
        return sum(
            float(z[0]) * mixed_unit_replacements[z[1]] for z in zip(v, u)
        )
    m = re.match(r'\[([^, ]*)[, ]*(.*)\]', s)
    if m.group(1).find('/') > -1:
        return reduce(lambda a, b: float(a) / float(b), m.group(1).split('/')) * mixed_unit_replacements[m.group(2)]
    return float(m.group(1)) * mixed_unit_replacements[m.group(2)]


def parse_entity(s):
    v = re.match(r'\["(.*)"[, ]*{(.*)}', s)
    return {
        'entity_type': v.group(1),
        'values': v.group(2).replace(' ', '').replace('"', '').split(',')
    }


def parse_date(s):
    m = re.match(
        r'\[{([0-9]*)[, ]*([0-9]*)[, ]*([0-9]*)[, ]*([0-9]*)[, ]*([0-9]*)[, ]*([0-9\.]*)}[, ]*"([^""]*)"[, ]*"([^""]*)"[, ]*([0-9 \.]*)',
        s
    )
    return {
        'year': int(m.group(1)),
        'month': int(m.group(2)),
        'day': int(m.group(3)),
        'hour': int(m.group(4)),
        'minute': int(m.group(5)),
        'second': float(m.group(6)),
        'type_a': m.group(7),
        'calendar_type': m.group(8),
        'additional_value': float(m.group(9))
    }


def parse_geo_position(s):
    m = re.match(r'\[{([\-0-9\.]*)[, ]*([\-0-9\.]*)', s)
    return {
        'lat': m.group(1),
        'lon': m.group(2)
    }


def entry_to_array(e):
    """
    Quantity in s
    =============

    {
        "type": "Quantity",
        "value": 67
    }

    DataObject
    ==========

    {
        'type': "DataObject"
    }

    GeoPosition
    ===========

    {
        'type': "GeoPosition",
        'values': {
            'lat': 31.12345,
            'lon': -31.12345
        }
    }

    Entity
    ======

    {
        'type': 'Entity',
        'entity': {
            'type': 'SomeEntityClass',
            'values': {
                'value_a': 123,
                'value_b': 'my content',
                ...
            }
        }
    }

    Missing
    =======

    None
    """
    m = re.match(r'([^\[]*)(.*)', e)
    t = m.group(1)
    if t == 'Quantity':
        return {
            'type': t,
            'value': parse_time_quantity(m.group(2))
        }
    elif t == 'Entity':
        return {
            'type': t,
            'entity': parse_entity(m.group(2))
        }
    elif t == 'DateObject':
        return {
            'type': t,
            'values': parse_date(m.group(2))
        }
    elif t == 'GeoPosition':
        return {
            'type': t,
            'values': parse_geo_position(m.group(2))
        }
    elif t == 'Missing':
        return None
    s = m.group(2).replace('0.', '0').replace("{", "[").replace("}", "]")
    return {
        'type': t,
        'values': s
    }